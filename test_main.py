import unittest

from main import *


class TestMain(unittest.TestCase):
    def test_if_test_is_working(self):
        result = test_function(1)
        expected = 2
        self.assertEqual(result, expected)

    def test_initial_functions_to_pass(self):
        # test all functions to return the result to safeguard the refactoring
        print(f"Preparing function tests \n{'*'*40}")

        # exercise_preparation
        test_name = "exercise_preparation"
        print(f'Starting test on function {test_name}')
        result = len(exercise_preparation(["python main.py", "3", "3", "4"])) > 0
        expected = True
        self.assertEqual(result, expected)

        # question_display
        test_name = "question_display"
        print(f'Starting test on function {test_name}')
        result = len(question_display(["1", "1", "1"], ["0", "1"])) > 0
        expected = True
        self.assertEqual(result, expected)

        # generate_exercise()
        test_name = "generate_exercise"
        print(f'Starting test on function {test_name}')
        output = question_generation(exercise_preparation(["python main.py", "3", "3", "4"]))
        result = output[1] > 0
        expected = True
        self.assertEqual(result, expected)

        # generate_gameover()
        result = generate_gameover()
        expected = ['Sveikinu tu pabaigei daugybos lentelę!!!', '', '', 'Valio']
        self.assertEqual(result, expected)

        # check_exercise
        result = check_exercise()
        expected = 0
        self.assertEqual(result, expected)

        # press_enter
        result = press_enter(None)
        expected = "Enter pressed"
        self.assertEqual(result, expected)

        # button_press
        result = button_press()
        expected = [game_over, "button pressed"]
        self.assertEqual(result, expected)

        # set_result
        result = set_result(50)
        expected = '        50%'
        self.assertEqual(result, expected)
