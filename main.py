# **************************************
# by Valdemaras Jakeliūnas, test python GUI project, 2020-05-20
# refactored and updated: 2020-10-10
# *************************************
# application arguments:
# first - first dimension of multiply table
# second - second dimension of multiply table
# third - number of exercises
# example of multiplication table of 8x8 with 20 exercises
# > python main.py 8 8 20

from tkinter import *
import random

# variables
rezultatas = 0
answer = 0
exercise_question_number = 0
game_over = False
no_wrong_answers = 0
no_correct_answers = 0
statistics = 0.0
exercises_all = []

# Execise preparation


def exercise_preparation(arg_cmd_args):
    # function that prepares the exercises list
    result_exercises_all = []

    if len(arg_cmd_args) >= 4:
        try:
            dimension_first = int(arg_cmd_args[1])
            dimension_second = int(arg_cmd_args[2])
            total_number_of_exercises = int(arg_cmd_args[3])
        except:
            dimension_first = 9
            dimension_second = 9
            total_number_of_exercises = 25

    else:
        dimension_first = 9
        dimension_second = 9
        total_number_of_exercises = 25

    i = 1
    while i <= dimension_first:
        j = 1
        while j <= dimension_second:
            result_exercises_all.append([i, j, i * j])
            j += 1
        i += 1

    while len(result_exercises_all) > total_number_of_exercises:
        question_to_drop = random.randint(0, len(result_exercises_all) - 1)
        result_exercises_all.pop(question_to_drop)

    return result_exercises_all


def question_display(arg_question, arg_exercises_all):
    # function that updates GUI element values to a new question values
    label_numbers["text"] = str(arg_question[0]) + "x" + str(arg_question[1])
    label_left_to_go_value["text"] = str(len(arg_exercises_all))
    return [label_numbers["text"], label_left_to_go_value["text"]]


def question_generation(arg_exercises_all):
    # question generation
    question_number = random.randint(0, len(arg_exercises_all) - 1)
    question = arg_exercises_all[question_number]

    # question display
    question_display(question, arg_exercises_all)

    # set global variables for checks and event-driven processing
    global answer
    answer = question[2]
    global exercise_question_number
    exercise_question_number = question_number

    return question[2], question_number     # returns for unit tests


def generate_gameover():
    label_answer["text"] = "Sveikinu tu pabaigei daugybos lentelę!!!"
    label_numbers["text"] = ""
    label_question["text"] = ""
    button_enter["text"] = "Valio"
    return [label_answer["text"],
            label_numbers["text"],
            label_question["text"],
            button_enter["text"]]   # for unit tests


def check_exercise():
    # check the answer

    global exercises_all
    global exercise_question_number
    global game_over
    global no_wrong_answers
    global no_correct_answers
    global statistics

    try:
        if int(entry_result.get()) == answer:
            no_correct_answers += 1
            entry_result.delete(0, END)
            label_answer["text"] = "Teisingai!!!"
            if len(exercises_all) > 1:
                exercises_all.pop(exercise_question_number)
            else:
                game_over = True
                generate_gameover()
                label_left_to_go_value["text"] = "0"
        else:
            no_wrong_answers += 1
            entry_result.delete(0, END)
            label_answer["text"] = "Neteisingai"
            label_wrong_value["text"] = str(no_wrong_answers)
    except:
        entry_result.delete(0, END)
        label_answer["text"] = "Neteisingai"
        no_wrong_answers += 1
        label_wrong_value["text"] = str(no_wrong_answers)

    if no_correct_answers > 0:
        statistics = 100 * no_correct_answers / (no_correct_answers + no_wrong_answers)
    else:
        statistics = 0
    set_result(statistics)
    return statistics   # for unit tests


def press_enter(event):
    # function for handling ENTER key press
    button_press()
    return "Enter pressed"   # for unit tests


def button_press():
    # function for handling button press
    if not game_over:
        check_exercise()
        question_generation(exercises_all)
    if game_over:
        generate_gameover()
    return [game_over, "button pressed"]   # for unit tests


def set_result(arg_result):
    nice_looking_result = "{:10.0f}".format(arg_result)
    label_result_value["text"] = str(nice_looking_result) + "%"
    return label_result_value["text"]   # for unit tests


def test_function(arg_number):
    return arg_number*2   # for unit tests


# Form composition and element definition

main_window = Tk()

# Form frames
panel_top = Frame(main_window, bd=10)
panel_almost_top = Frame(main_window, bd=10)
panel_center = Frame(main_window, bd=10)
panel_almost_bottom = Frame(main_window)
panel_bottom_left = Frame(panel_almost_bottom, bd=10)
panel_bottom_right = Frame(panel_almost_bottom, bd=10)
panel_bottom_bottom = Frame(main_window, bd=10)

# exercise row
label_question = Label(panel_top, text="Kiek bus: ")
label_numbers = Label(panel_top, text="0 x 0")
entry_result = Entry(panel_top, width=30)
entry_result.bind("<Return>", press_enter)
button_enter = Button(panel_top, text="Įvesti", command=button_press)

label_answer = Label(panel_center, text=" "*100)

# statistics
# Left to go
label_left_to_go_txt = Label(panel_bottom_left, text="Liko:")
label_left_to_go_value = Label(panel_bottom_left, text=0)

# Wrong answers
label_wrong_txt = Label(panel_bottom_right, text="Iš jų pakartotinai (neteisingai pirmą kartą):")
label_wrong_value = Label(panel_bottom_right, text=0)
# results
label_result_txt = Label(panel_bottom_bottom, text="Rezultatas: ")
label_result_value = Label(panel_bottom_bottom, text=str(statistics) + "%")

# packing
label_question.pack(side=LEFT)
label_numbers.pack(side=LEFT)
entry_result.pack(side=LEFT)
button_enter.pack(side=LEFT)

label_answer.pack()

label_left_to_go_txt.pack()
label_left_to_go_value.pack()

label_wrong_txt.pack()
label_wrong_value.pack()

label_result_txt.pack(side=LEFT)
label_result_value.pack(side=LEFT)

panel_top.pack(side=TOP)
panel_almost_top.pack(side=TOP)
panel_center.pack(side=TOP)
panel_bottom_left.pack(side=LEFT)
panel_bottom_right.pack(side=RIGHT)
panel_bottom_bottom.pack(side=BOTTOM)
panel_almost_bottom.pack(side=BOTTOM)

# exercise preparation
exercises_all = exercise_preparation(sys.argv)
# first question generation
answer, exercise_question_number = question_generation(exercises_all)

# first question display
set_result(0)

# check whether application is being run directly
# if executed from outside (like unit tests) application will not run
if __name__ == '__main__':
    main_window.mainloop()
