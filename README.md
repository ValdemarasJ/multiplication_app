# Multiplication app for children #

### a) First project to utilize tkinter  ###
### b) Application refactoring demo ###
***
### Process planned: ###
* Initial - green (original) project uploaded **[DONE]**
* First step - define unit test for all main functions available in the initial version to ensure application will not be broken during refactoring **[DONE]**
* Second step - refactore the application (leaving the same functionality) - structure and use better coding practices **[DONE]**
* Third step - update the application to have a cleaner GUI ***[IN PROGRESS]***
* Last step - multilanguage - parametrize all interface text in language files [PLANNED]

### Application command line arguments: ###
* first - first dimension of multiply table
* second - second dimension of multiply table
* third - number of exercises
* example of multiplication table of 8x8 with 20 exercises
* '> python main.py 8 8 20'

### For windows (though application possibly could be packed for any python app) preconfigured cmd files - to run specific set of exercises:###
* "9x9 - 10 excs.cmd" - 9x9 multiplication table, 10 random exercises
* "9x9 - 25 excs.cmd" - 9x9 multiplication table, 25 random exercises